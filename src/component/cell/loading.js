import React, {useState, useEffect, useRef} from 'react';
import {View, Text, Image, TouchableOpacity, Animated} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

const Loading = ({text, color}) => {
  const height = useRef(new Animated.Value(0)).current;
  const opacity = useRef(new Animated.Value(0.2)).current;
  useEffect(() => {
    Animated.parallel([
      Animated.loop(
        Animated.timing(height, {
          toValue: 1,
          duration: 1000,
          useNativeDriver: true,
        }),
      ),
      Animated.loop(
        Animated.timing(opacity, {
          toValue: 1,
          duration: 1000,
          useNativeDriver: true,
        }),
      ),
    ]).start();
  }, []);
  return (
    <View style={{justifyContent: 'center', alignItems: 'center'}}>
      <Animated.View
        style={{
          height: 50,
          width: 50,
          transform: [{scaleX: height}, {scaleY: height}],
          backgroundColor: color ? color : 'purple',
          borderRadius: 100,
          opacity: opacity,
        }}></Animated.View>
      {text && (
        <Text style={{margin: 20, fontSize: 15, fontWeight: '700'}}>
          {text}
        </Text>
      )}
    </View>
  );
};

export default Loading;
