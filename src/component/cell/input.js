import React, { useState, useEffect } from "react";
import { View, Text, Image, Pressable, TextInput } from "react-native";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import { color } from "../../utilities/json";

const Input = ({
  placeholder,
  password,
  value,
  onChange = () => {},
  icons,
  onFocus = () => {},
  noBorder,
  type,
  label,
  height,
  disable = true,
}) => {
  const [borderColor, setBorderColor] = useState(false);
  const [SeePassword, setSeePassword] = useState(false);
  return (
    <View style={{ marginVertical: 10 }}>
      {label && <Text style={{ color: color.primary }}>{label}</Text>}
      <View
        style={{
          elevation: 3,
          backgroundColor: disable ? "white" : "#e8e8e8",
          borderRadius: 10,
          borderWidth: borderColor ? 1 : 0.5,
          borderColor: borderColor ? color.primary : color.Grey,
          paddingHorizontal: 7,
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        {icons && (
          <FontAwesome5
            name={icons}
            size={15}
            solid={true}
            color={borderColor ? color.primary : color.Grey}
            style={{ marginRight: 5 }}
          />
        )}
        <TextInput
          placeholder={placeholder}
          secureTextEntry={SeePassword ? false : password}
          value={value}
          editable={disable}
          onFocus={() => {
            setBorderColor(true), onFocus();
          }}
          keyboardType={type ? type : "default"}
          placeholderTextColor="grey"
          autoCapitalize={"none"}
          onEndEditing={() => setBorderColor(false)}
          onChangeText={(e) => onChange(e)}
          style={{
            flex: 1,
            fontFamily: "OpenSans-Regular",
            color: disable ? "black" : "grey",
          }}
        />
        {password && (
          <Pressable onPress={() => setSeePassword(!SeePassword)}>
            <FontAwesome5
              name={!SeePassword ? "eye" : "eye-slash"}
              size={15}
              color={color.primary}
            />
          </Pressable>
        )}
      </View>
    </View>
  );
};

export default Input;
