import React, { useState, useEffect, useRef } from "react";
import {
  View,
  Text,
  Image,
  Pressable,
  TextInput,
  Animated,
} from "react-native";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import Button from "./button";
import { color } from "../../utilities/json";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
const Modal = ({
  onClosed: [state, setState],
  title,
  children,
  actionButton = () => {},
  titleButton,
  cancelButton,
  onPressActionButton = () => {},
  hiddenClose,
}) => {
  const scale = useRef(new Animated.Value(0)).current;
  const opacity = useRef(new Animated.Value(0)).current;
  useEffect(() => {
    if (state) {
      Animated.parallel([
        Animated.timing(scale, {
          toValue: 1,
          duration: 100,
          useNativeDriver: true,
        }),
        Animated.timing(opacity, {
          toValue: 0.5,
          duration: 100,
          useNativeDriver: true,
        }),
      ]).start();
    }
  }, [state]);

  const setFalse = () => {
    setTimeout(() => {
      setState(false);
    }, 100);
    Animated.parallel([
      Animated.timing(scale, {
        toValue: 0,
        duration: 100,
        useNativeDriver: true,
      }),
      Animated.timing(opacity, {
        toValue: 0,
        duration: 100,
        useNativeDriver: true,
      }),
    ]).start();
  };
  return (
    <View
      style={{
        height: state ? "100%" : 0,
        width: state ? "100%" : 0,
        position: "absolute",
        zIndex: state ? 8 : 0,
      }}
    >
      {state && (
        <>
          <View
            style={{
              height: "100%",
              width: "100%",
              position: "absolute",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            {/* <Animated.View
              style={{
                height: "100%",
                width: "100%",
                position: "absolute",
                backgroundColor: "black",
                opacity: opacity,
                zIndex: 9,
              }}
            /> */}
            <Animated.View
              style={{
                width: "80%",
                padding: 10,
                backgroundColor: "white",
                borderRadius: 3,
                zIndex: 10,
                elevation: 10,
                transform: [{ scaleX: scale }, { scaleY: scale }],
              }}
            >
              <View
                style={{
                  alignItems: "flex-end",
                  flexDirection: "row",
                  justifyContent: "space-between",
                }}
              >
                <View />
                {title ? (
                  <Text
                    style={{
                      color: color.dragGrey,
                      fontFamily: "OpenSans-SemiBold",
                      fontSize: 14,
                    }}
                  >
                    {title}
                  </Text>
                ) : (
                  <View />
                )}
                {!hiddenClose ? (
                  <Pressable onPress={() => setFalse()}>
                    <FontAwesome5 name="times" size={20} color={color.Grey} />
                  </Pressable>
                ) : (
                  <View />
                )}
              </View>
              <View style={{ marginVertical: 20, paddingHorizontal: 10 }}>
                {children}
              </View>
              {titleButton && (
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  <View style={{ flex: 0.45 }}>
                    <Button
                      title={cancelButton}
                      color={color.error}
                      small={true}
                      borderRadius={5}
                      fontSize={15}
                      onPress={() => {
                        setFalse();
                      }}
                    />
                  </View>
                  <View style={{ flex: 0.45 }}>
                    <Button
                      title={titleButton}
                      color={color.primary}
                      small={true}
                      borderRadius={5}
                      fontSize={15}
                      onPress={() => {
                        setFalse();
                        actionButton();
                      }}
                    />
                  </View>
                </View>
              )}
            </Animated.View>
          </View>
        </>
      )}
    </View>
  );
};

export default Modal;
