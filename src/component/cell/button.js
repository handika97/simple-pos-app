import React, { useState, useEffect, useRef } from "react";
import {
  View,
  Text,
  Image,
  Pressable,
  TextInput,
  Animated,
  Dimensions,
  ActivityIndicator,
} from "react-native";

const Button = ({
  title,
  onPress = () => {},
  small,
  style,
  color,
  loading = false,
  colorLoading = "white",
  borderRadius,
  fontSize,
  padding,
}) => {
  const [x, setX] = useState(0);
  const [Active, setActive] = useState(false);
  const scaleX = useRef(new Animated.Value(-x)).current;
  const opacity = useRef(new Animated.Value(0)).current;
  const windowWidth = Dimensions.get("window").width;
  const windowHeight = Dimensions.get("window").height;
  const buttonOn = () => {
    Animated.parallel([
      Animated.timing(scaleX, {
        toValue: 0,
        duration: 300,
        useNativeDriver: true,
      }),
      Animated.timing(opacity, {
        toValue: 0.5,
        duration: 0,
        useNativeDriver: true,
      }),
    ]).start();
    setTimeout(() => {
      buttonOff();
    }, 300);
  };
  const buttonOff = () => {
    Animated.parallel([
      Animated.timing(scaleX, {
        toValue: -x,
        duration: 100,
        useNativeDriver: true,
      }),
    ]).start();
    setActive(false);
  };
  return (
    <View>
      <Pressable
        style={{
          backgroundColor: color,
          marginVertical: 10,
          borderRadius: borderRadius ? borderRadius : 100,
          elevation: 2,
          alignItems: "center",
          justifyContent: "center",
        }}
        onPress={() => {
          loading ? "" : onPress();
        }}
        // onPressIn={() => {
        //   buttonOn();
        //   setActive(true);
        // }}
        onLayout={(event) => {
          setX(event.nativeEvent.layout.width);
        }}
      >
        <View
          style={{
            height: 45,
            padding: small ? 5 : padding ? padding : 8,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          {loading ? (
            <ActivityIndicator color={colorLoading} size="small" />
          ) : (
            <Text
              style={{
                color: "white",
                fontSize: fontSize ? fontSize : 18,
                fontFamily: "OpenSans-SemiBold",
              }}
            >
              {title}
            </Text>
          )}
        </View>
        <Animated.View
          style={{
            height: "100%",
            width: Active ? x : 0,
            transform: [{ translateX: scaleX }],
            position: "absolute",
            borderRadius: 50,
            alignSelf: "flex-start",
            backgroundColor: "white",
            opacity: opacity,
          }}
        ></Animated.View>
      </Pressable>
    </View>
  );
};

export default Button;
