import React, { useState, useEffect, useRef, useCallback } from "react";
import {
  View,
  Image,
  Text,
  ScrollView,
  SafeAreaView,
  StatusBar,
  FlatList,
  Pressable,
  ImageBackground,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
const wait = (timeout) => {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout);
  });
};

const AccountList = ({ navigation, route }) => {
  const dispatch = useDispatch();
  const Auth = useSelector((state) => state.auth);
  const [loading, setLoading] = useState(false);
  const [errorMessage, seterrorMessage] = useState("");
  const [Error, setError] = useState(false);
  const [attribute, setAttribute] = useState({
    username: "",
    password: "",
  });

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: "white",
      }}
    >
      <StatusBar
        barStyle="light-content"
        hidden={false}
        backgroundColor="#21A49F"
        translucent={false}
        networkActivityIndicatorVisible={true}
      />
      <ImageBackground
        source={require("../../assets/images/background.png")}
        style={{
          width: wp("100%"),
          height: hp("100%"),
          alignItems: "center",
          zIndex: 0,
        }}
      >
        <ScrollView
          style={{
            marginTop: hp("7%"),
            width: wp("90%"),
            marginTop: hp("2.5%"),
            backgroundColor: "white",
            padding: wp("3%"),
            borderRadius: 5,
            zIndex: 1,
            elevation: 2,
          }}
        >
          <FlatList
            data={route.params.data}
            renderItem={({ item }) => {
              return (
                <Pressable
                  style={{
                    padding: 20,
                    marginVertical: 2,
                    borderBottomWidth: 0.7,
                    borderColor: "#02b3a6",
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                  onPress={() => {
                    route.params.callback(item), navigation.goBack();
                  }}
                >
                  <View
                    style={{ justifyContent: "center", alignItems: "center" }}
                  >
                    <Text
                      style={{
                        color: "black",
                        fontFamily: "OpenSans-SemiBold",
                        fontSize: 13,
                        textAlign: "center",
                      }}
                    >
                      {item.label}
                    </Text>
                  </View>
                </Pressable>
              );
            }}
            keyExtractor={(item) => item.id}
            style={{ paddingHorizontal: "7.5%", marginBottom: 20 }}
          />
        </ScrollView>
      </ImageBackground>
    </View>
  );
};

export default AccountList;
