import React, { useState, useEffect, useRef } from "react";
import { View, Text, Image, Alert, Pressable } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { color } from "../../utilities/json";
import { authLogOut } from "../../redux/features/authSlice";
import { sayTime, RupiahFormat } from "../../utilities/Function";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";

const Header = ({ navigation }) => {
  const { data } = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  return (
    <View
      style={{
        flexDirection: "row",
        alignItems: "flex-start",
        paddingHorizontal: wp(5),
        paddingTop: wp(5) + 30,
        paddingBottom: wp(5) + 40,
        backgroundColor: color.primary,
        height: hp(20),
        alignItems: "center",
        justifyContent: "space-between",
      }}
    >
      <View
        style={{
          flexDirection: "row",
          alignItems: "flex-start",
        }}
      >
        <View
          style={{
            width: wp("15"),
            height: wp("15"),
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: color.white,
            elevation: 2,
            borderRadius: 4,
            padding: 5,
          }}
        >
          <Image
            source={require("../../assets/images/logo_app.jpg")}
            style={{ width: "100%", height: "100%" }}
          />
        </View>
        <View
          style={{
            flex: 0.7,
            marginLeft: 10,
          }}
        >
          <Text
            style={{
              fontFamily: "OpenSans-Bold",
              fontSize: wp(4),
              textTransform: "capitalize",
              color: color.secondary,
            }}
          >
            {sayTime()}
          </Text>
          <Text
            style={{
              fontFamily: "OpenSans-Bold",
              fontSize: wp(4.5),
              textTransform: "capitalize",
              color: color.white,
            }}
          >
            {data.firstName}
          </Text>
        </View>
      </View>
      <Pressable
        onPress={() =>
          Alert.alert("Logout", "Are you sure you want to Logout?", [
            {
              text: "No",
              onPress: () => {},
              style: "cancel",
            },
            { text: "Yes", onPress: () => dispatch(authLogOut()) },
          ])
        }
      >
        <FontAwesome5 name="power-off" size={25} />
      </Pressable>
    </View>
  );
};

export default Header;
