import React, { useState, useEffect, useRef } from "react";
import {
  SafeAreaView,
  Image,
  Text,
  View,
  ImageBackground,
  StatusBar,
  Animated,
  Easing,
  Linking,
  ToastAndroid,
} from "react-native";
import { color } from "../../utilities/json";
import { Input, Button } from "../../component/cell";
import { useDispatch, useSelector } from "react-redux";
import { post, getCustom, get } from "../../redux/features/main";
import { authLoginReceive } from "../../redux/features/authSlice";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import Pressable from "react-native/Libraries/Components/Pressable/Pressable";

const SplashScreen = ({ navigation }) => {
  const translateY = useRef(new Animated.Value(0)).current;
  const translateYContainer = useRef(new Animated.Value(hp("65"))).current;
  const dispatch = useDispatch();
  const [loading, setloading] = useState(false);
  const [attribute, setAttribute] = useState({
    username: "",
    password: "",
  });

  const Login = async () => {
    if (validation()) {
      setloading(true);
      dispatch(
        post(
          "/api/users/login",
          { username: attribute.username, password: attribute.password },
          (res) => {
            setloading(false);
            dispatch(
              authLoginReceive({
                ...res.data.data,
                login_at: new Date(),
              })
            );
          },
          (err) => {
            toastWithDurationHandler(
              err.response?.message
                ? err.response?.message
                : "Oooppss Something Wrong"
            );
          },
          () => setloading(false)
        )
      );
    }
  };

  const validation = () => {
    if (!attribute.username) {
      toastWithDurationHandler("Masukan username/phone number Anda");
      return false;
    }
    if (!attribute.password) {
      toastWithDurationHandler("Masukan Password Anda");
      return false;
    }
    return true;
  };

  const toastWithDurationHandler = (message) => {
    // To make Toast with duration
    ToastAndroid.show(message, ToastAndroid.SHORT);
  };

  const getDataRef = async () => {
    dispatch(
      getCustom(
        `https://koperasiumkm.bgrlogistics.id/rest_ci_wp/index.php/wpagen/get?id_mitra=1`,
        (res) => {},
        (err) => {},
        () => {
          //   setloading(false);
        }
      )
    );
  };

  useEffect(() => {
    getDataRef();
  }, []);

  useEffect(() => {
    Animated.timing(translateY, {
      toValue: -hp(33),
      duration: 500,
      useNativeDriver: true,
    }).start();
    Animated.timing(translateYContainer, {
      toValue: 0,
      duration: 500,
      useNativeDriver: true,
    }).start();
  }, []);

  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: color.white,
      }}
    >
      <StatusBar
        barStyle="light-content"
        hidden={false}
        backgroundColor={color.primary}
        translucent={true}
        networkActivityIndicatorVisible={true}
      />
      <View
        style={{
          flex: 1,
          backgroundColor: color.primary,
          width: wp("100%"),
          height: hp("100%"),
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Animated.View
          style={{
            padding: wp("5"),
            // translateY,
            backgroundColor: color.white,
            borderRadius: 100,
            width: wp("35"),
            height: wp("35"),
            transform: [{ translateY: translateY }],
          }}
        >
          <Image
            source={require("../../assets/images/logo_app.jpg")}
            style={{ width: "100%", height: "100%" }}
          />
        </Animated.View>
        <Animated.View
          style={{
            width: wp("100%"),
            height: hp("65"),
            bottom: 0,
            backgroundColor: color.white,
            borderTopLeftRadius: wp("10"),
            borderTopRightRadius: wp("10"),
            elevation: 5,
            position: "absolute",
            transform: [{ translateY: translateYContainer }],
            padding: wp("10"),
          }}
        >
          <Text
            style={{
              fontFamily: "OpenSans-Bold",
              fontSize: wp(5.5),
              color: color.primary,
              marginTop: hp(1),
            }}
          >
            Simple POS
          </Text>
          <Text
            style={{
              fontFamily: "OpenSans-SemiBold",
              fontSize: wp(4.7),
              textTransform: "capitalize",
              color: color.secondary,
              marginTop: hp(1),
            }}
          >
            Selamat Datang
          </Text>
          <View
            style={{
              width: "100%",
              marginTop: hp(3),
            }}
          >
            <Input
              placeholder="username/phone number"
              value={attribute.username}
              icons="user"
              type="email-address"
              onChange={(e) => setAttribute({ ...attribute, username: e })}
            />
          </View>
          <View
            style={{
              width: "100%",
            }}
          >
            <Input
              placeholder="password"
              password={true}
              value={attribute.password}
              icons="lock"
              type="default"
              onChange={(e) => setAttribute({ ...attribute, password: e })}
            />
          </View>
          <View
            style={{
              width: "100%",
              marginTop: hp("5%"),
            }}
          >
            <Button
              title="Masuk"
              color="#21A49F"
              borderRadius={10}
              loading={loading}
              padding={10}
              onPress={() => {
                Login();
              }}
            />
          </View>
          <Text
            style={{
              color: "grey",
              fontFamily: "OpenSans-SemiBold",
              textAlign: "center",
              fontSize: 14.5,
              marginTop: hp(1.5),
            }}
          >
            ©2022 - Build with{" "}
            <FontAwesome5 name="heart" color={"red"} size={14.5} solid={true} />{" "}
            by
          </Text>
          <Text
            style={{
              color: color.secondary,
              fontFamily: "OpenSans-SemiBold",
              textAlign: "center",
              fontSize: 14,
            }}
          >
            Handika YK
          </Text>
        </Animated.View>
      </View>
    </SafeAreaView>
  );
};

export default SplashScreen;
