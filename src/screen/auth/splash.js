import React, { useState, useEffect, useRef } from "react";
import {
  SafeAreaView,
  Image,
  Text,
  View,
  ImageBackground,
  StatusBar,
  Animated,
  Easing,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { color } from "../../utilities/json";
import { useDispatch, useSelector } from "react-redux";
import { authLogOut } from "../../redux/features/authSlice";
import { getCustom } from "../../redux/features/main";

const SplashScreen = ({ navigation }) => {
  const opacityValue = useRef(new Animated.Value(0)).current;
  const translateX = useRef(new Animated.Value(10)).current;
  const [loading, setloading] = useState(0);
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  useEffect(() => {
    Animated.timing(opacityValue, {
      toValue: 1,
      duration: 2000,
      useNativeDriver: true,
      easing: Easing.in(Easing.bounce),
    }).start();
    translateX.addListener(({ value }) => {
      setloading((value * wp("80%")) / 100);
    });
    Animated.timing(translateX, {
      toValue: 30,
      duration: 3000,
      useNativeDriver: true,
    }).start();
    setTimeout(() => {
      setTimeout(() => {
        Animated.timing(translateX, {
          toValue: 70,
          duration: 1000,
          useNativeDriver: true,
        }).start();
        setTimeout(() => {
          setTimeout(() => {
            Animated.timing(translateX, {
              toValue: 100,
              duration: 1000,
              useNativeDriver: true,
            }).start();
          }, 1000);
        }, 500);
      }, 1000);
    }, 1000);
  }, []);

  const getDataRef = async () => {
    dispatch(
      getCustom(
        `https://koperasiumkm.bgrlogistics.id/rest_ci_wp/index.php/wpagen/get?id_mitra=1`,
        (res) => {},
        (err) => {},
        () => {
          //   setloading(false);
        }
      )
    );
  };

  useEffect(() => {
    getDataRef();
  }, []);

  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: color.white,
      }}
    >
      <StatusBar
        barStyle="light-content"
        hidden={false}
        backgroundColor={color.primary}
        translucent={true}
        networkActivityIndicatorVisible={true}
      />
      <View
        style={{
          flex: 1,
          backgroundColor: color.primary,
          width: wp("100%"),
          height: hp("100%"),
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Animated.View
          style={{
            padding: wp("5"),
            // opacityValue,
            backgroundColor: color.white,
            borderRadius: 100,
            opacity: opacityValue,
            width: wp("35"),
            height: wp("35"),
            transform: [{ scale: opacityValue }],
          }}
        >
          <Image
            source={require("../../assets/images/logo_app.jpg")}
            style={{ width: "100%", height: "100%" }}
          />
        </Animated.View>
        <View
          style={{
            width: wp("80%"),
            height: 7,
            bottom: 40,
            backgroundColor: color.white,
            borderRadius: 50,
            position: "absolute",
          }}
        >
          <Animated.View
            style={{
              width: loading,
              height: 7,
              backgroundColor: color.primary,
              borderRadius: 50,
              //   transform: [{translateX: translateX}],
            }}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default SplashScreen;
