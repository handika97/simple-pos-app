import React, { useState, useEffect, useRef } from "react";
import {
  View,
  Text,
  Pressable,
  FlatList,
  ScrollView,
  RefreshControl,
  SafeAreaView,
  StatusBar,
  Image,
  Animated,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { Input, Modal } from "../../../component/cell";
import { color } from "../../../utilities/json";
import { get } from "../../../redux/features/main";
import {
  setPlus,
  setMinus,
  setClean,
} from "../../../redux/features/orderSlice";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import Header from "../../../component/atoms/Header";

const MenuScreen = ({ navigation }) => {
  const translateY = useRef(new Animated.Value(0)).current;
  const { orderList, totalOrder } = useSelector((state) => state.orders);
  const dispatch = useDispatch();
  const [refreshing, setRefreshing] = useState(false);
  const [list, setList] = useState([]);
  const [search, setSearch] = useState("");
  const [activeModal, setActiveModal] = useState(false);
  const [selectedItem, setSelectedItem] = useState({
    code: null,
    name: null,
  });

  const getItemLayout = (data, index) => ({
    length: data.length,
    offset: data.length * index,
    index,
  });

  const getData = () => {
    setRefreshing(true);
    dispatch(
      get(
        "/api/orders/",
        (res) => {
          setList(res.data.data);
        },
        (err) => {
          console.log(err);
        },
        () => {
          setRefreshing(false);
        }
      )
    );
  };

  useEffect(() => {
    getData();
    dispatch(setClean());
  }, []);

  const onRefresh = () => {
    setRefreshing(true);
    getData();
  };

  useEffect(() => {
    if (totalOrder > 0) {
      Animated.timing(translateY, {
        toValue: 0,
        duration: 200,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(translateY, {
        toValue: wp(14) + 30,
        duration: 200,
        useNativeDriver: true,
      }).start();
    }
  }, [totalOrder]);

  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: "white",
      }}
    >
      <StatusBar
        barStyle="light-content"
        hidden={false}
        backgroundColor={"transparent"}
        translucent={true}
        networkActivityIndicatorVisible={true}
      />
      <Header />
      <View
        style={{
          borderTopRightRadius: 20,
          borderTopLeftRadius: 20,
          backgroundColor: color.white,
          marginTop: -25,
          width: wp(100),
          height: hp(80),
        }}
      >
        <View
          style={{
            alignItems: "center",
            marginHorizontal: wp(5),
          }}
        >
          <View style={{ width: "100%" }}>
            <Input
              placeholder={"Cari Menu"}
              value={search}
              onChange={(e) => {
                setSearch(e);
              }}
              icons="search"
            />
          </View>
        </View>
        <View style={{ height: hp(1) }} />
        <ScrollView
          style={{ width: "100%" }}
          keyboardShouldPersistTaps="always"
          refreshControl={
            <RefreshControl onRefresh={onRefresh} refreshing={refreshing} />
          }
        >
          {!refreshing && (
            <FlatList
              data={list.filter(
                (item) =>
                  item.code?.toLowerCase().indexOf(search.toLowerCase()) >= 0 ||
                  item.name?.toLowerCase().indexOf(search.toLowerCase()) >= 0
              )}
              renderItem={({ item, i }) => {
                return (
                  <Pressable
                    style={{
                      backgroundColor: color.white,
                      elevation: 2,
                      marginVertical: 4,
                      marginHorizontal: wp(5),
                      borderRadius: 3,
                      flexDirection: "row",
                    }}
                    onPress={() => {
                      setActiveModal(true);
                      setSelectedItem(item);
                    }}
                  >
                    <View
                      style={{
                        width: wp(30),
                        height: wp(30),
                      }}
                    >
                      <Image
                        source={require("../../../assets/images/bakpao_coklat.jpg")}
                        style={{
                          width: "100%",
                          height: "100%",
                          borderTopLeftRadius: 3,
                          borderBottomLeftRadius: 3,
                        }}
                        resizeMode={"cover"}
                      />
                    </View>
                    <View style={{ padding: 10 }}>
                      <Text
                        style={{
                          fontFamily: "OpenSans-Bold",
                          fontSize: wp(3),
                          color: color.secondary,
                        }}
                      >
                        {item.code}
                      </Text>
                      <Text
                        style={{
                          fontFamily: "OpenSans-Bold",
                          fontSize: wp(4),
                          color: color.primary,
                        }}
                      >
                        {item.name}
                      </Text>
                    </View>
                    {orderList.filter((e, i) => {
                      return e.menu_food_id == item.id;
                    }).length > 0 && (
                      <View
                        style={{
                          position: "absolute",
                          width: 30,
                          height: 30,
                          borderRadius: 30,
                          justifyContent: "center",
                          alignItems: "center",
                          backgroundColor: color.secondary,
                          top: 0,
                          right: 0,
                          elevation: 2,
                        }}
                      >
                        <Text
                          style={{
                            fontFamily: "OpenSans-Bold",
                            fontSize: wp(4),
                            color: color.white,
                          }}
                        >
                          {orderList.filter((e, i) => {
                            return e.menu_food_id == item.id;
                          }).length > 0
                            ? orderList.filter((e, i) => {
                                return e.menu_food_id == item.id;
                              })[0].qty
                            : 0}
                        </Text>
                      </View>
                    )}
                  </Pressable>
                );
              }}
              getItemLayout={getItemLayout}
              initialNumToRender={5}
              maxToRenderPerBatch={10}
              ListEmptyComponent={
                <View
                  style={{ width: "100%", alignItems: "center", marginTop: 70 }}
                >
                  <Image
                    resizeMode="contain"
                    source={require("../../../assets/images/no-results.png")}
                    style={{ width: "70%", height: hp("20") }}
                  />
                </View>
              }
              windowSize={10}
              keyExtractor={(item) => item.no_wo}
            />
          )}
          <View style={{ height: 50 }} />
        </ScrollView>
      </View>
      <Modal
        onClosed={[activeModal, setActiveModal]}
        title={selectedItem.code + " - " + selectedItem.name}
      >
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Pressable
            onPress={() =>
              dispatch(
                setMinus({
                  id: selectedItem.id,
                })
              )
            }
          >
            <FontAwesome5 name="minus-circle" size={wp(10)} />
          </Pressable>
          <Text
            style={{
              fontFamily: "OpenSans-Bold",
              fontSize: wp(10),
              color: color.primary,
            }}
          >
            {orderList.filter((item, i) => {
              return item.menu_food_id == selectedItem.id;
            }).length > 0
              ? orderList.filter((item, i) => {
                  return item.menu_food_id == selectedItem.id;
                })[0].qty
              : 0}
          </Text>
          <Pressable
            onPress={() =>
              dispatch(
                setPlus({
                  id: selectedItem.id,
                  code: selectedItem.code,
                  name: selectedItem.name,
                  food_price: selectedItem.food_price,
                })
              )
            }
          >
            <FontAwesome5 name="plus-circle" size={wp(10)} />
          </Pressable>
        </View>
      </Modal>
      <Animated.View
        style={{
          position: "absolute",
          width: "80%",
          height: wp(14),
          borderRadius: 100,
          backgroundColor: "white",
          elevation: 4,
          bottom: 20,
          alignSelf: "center",
          borderColor: color.secondary,
          borderWidth: 1,
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
          padding: 6,
          transform: [{ translateY: translateY }],
        }}
      >
        <FontAwesome5 name="shopping-basket" size={wp(7)} />
        <Text
          style={{
            fontFamily: "OpenSans-Bold",
            fontSize: wp(7),
            color: color.primary,
          }}
        >
          {totalOrder}
        </Text>
        <Pressable
          onPress={() => {
            navigation.navigate("CartScreen");
          }}
        >
          <FontAwesome5
            name="arrow-alt-circle-right"
            size={wp(7)}
            solid={true}
          />
        </Pressable>
      </Animated.View>
    </SafeAreaView>
  );
};

export default MenuScreen;
