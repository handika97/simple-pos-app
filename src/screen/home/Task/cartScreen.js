import React, { useState, useEffect, useRef } from "react";
import {
  View,
  Text,
  Pressable,
  ScrollView,
  SafeAreaView,
  StatusBar,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { Button, Modal } from "../../../component/cell";
import { color } from "../../../utilities/json";
import { post } from "../../../redux/features/main";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import Header from "../../../component/atoms/Header";
import { RupiahFormat } from "../../../utilities/Function";
import { service_type, payment_type } from "../../../utilities/json";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { setClean } from "../../../redux/features/orderSlice";
import Snackbar from "react-native-snackbar";

const Picker = ({ title = null, onPress = () => {}, value }) => {
  return (
    <>
      <Text
        style={{
          color: "black",
          fontFamily: "OpenSans-Bold",
          fontSize: wp("3.7"),
          marginBottom: 10,
        }}
      >
        {title}
      </Text>
      <View style={{ paddingHorizontal: 4 }}>
        <Pressable
          style={{
            elevation: 3,
            backgroundColor: "white",
            borderRadius: 10,
            borderWidth: 1,
            height: 47,
            borderColor: color.Grey,
            paddingHorizontal: 7,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
          }}
          onPress={onPress}
        >
          <Text
            style={{
              color: "black",
              fontFamily: "OpenSans-Regular",
              fontSize: wp("3.7"),
            }}
          >
            {value}
          </Text>
          <FontAwesome5 name="caret-down" size={22} color={color.secondary} />
        </Pressable>
      </View>
    </>
  );
};
const PriceDetail = ({ title = null, value = null }) => {
  return (
    <View style={{ flexDirection: "row", marginVertical: 3 }}>
      <Text style={{ flex: 0.4, fontFamily: "OpenSans-SemiBold" }}>
        {title}
      </Text>
      <Text style={{ flex: 0.25 }}></Text>
      <Text style={{ flex: 0.35 }}>{value}</Text>
    </View>
  );
};

const RadioButton = ({ title, value, data, onPress = () => {} }) => {
  return (
    <Pressable
      style={{
        flexDirection: "row",
        marginTop: 10,
      }}
      onPress={() => onPress(value)}
    >
      <View
        style={{
          flex: 0.1,
        }}
      >
        <MaterialCommunityIcons
          color={color.primary}
          name={
            data == value ? "checkbox-intermediate" : "checkbox-blank-outline"
          }
          size={20}
        />
      </View>
      <Text
        style={{
          color: data == value ? "black" : color.Grey,
          fontFamily: "OpenSans-SemiBold",
          fontSize: wp("3.7"),
          flex: 0.9,
        }}
      >
        {title}
      </Text>
    </Pressable>
  );
};

const OrderType = ({ navigation }) => {
  const { orderList } = useSelector((state) => state.orders);
  const dispatch = useDispatch();
  const [activeModal, setActiveModal] = useState(false);
  const [loading, setLoading] = useState(false);
  const [attribute, setAttribute] = useState({
    service_type: 1,
    payment_type: 1,
    type_modal: null,
  });

  const order = () => {
    setLoading(true);
    dispatch(
      post(
        "/api/orders/",
        {
          service_type: attribute.service_type,
          payment_type: attribute.payment_type,
          order_list: orderList,
        },
        (res) => {
          navigation.goBack();
          dispatch(setClean());
          Snackbar.show({
            text: "Success for order",
            duration: Snackbar.LENGTH_SHORT,
            backgroundColor: "green",
            fontFamily: "OpenSans-Bold",
          });
        },
        (err) => {
          Snackbar.show({
            text: "Oppssss something wrong",
            duration: Snackbar.LENGTH_SHORT,
            backgroundColor: "red",
            fontFamily: "OpenSans-Bold",
          });
        },
        () => {
          setLoading(false);
        }
      )
    );
  };

  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: "white",
      }}
    >
      <StatusBar
        barStyle="light-content"
        hidden={false}
        backgroundColor={"transparent"}
        translucent={true}
        networkActivityIndicatorVisible={true}
      />
      <Header />
      <View
        style={{
          borderTopRightRadius: 20,
          borderTopLeftRadius: 20,
          backgroundColor: color.white,
          marginTop: -25,
          width: wp(100),
          height: hp(80),
        }}
      >
        <View style={{ height: hp(1) }} />
        <ScrollView
          style={{ width: "100%" }}
          keyboardShouldPersistTaps="always"
        >
          <View style={{ paddingHorizontal: 10 }}>
            <Picker
              title={"Service Type"}
              onPress={() => {
                setActiveModal(true);
                setAttribute({ ...attribute, type_modal: "service_type" });
              }}
              value={
                service_type.filter((e) => {
                  return e.id === attribute.service_type;
                })[0].name
              }
            />
          </View>
          <View
            style={{
              height: 5,
              backgroundColor: color.ligthGrey,
              width: "100%",
              marginVertical: 20,
            }}
          />
          <View style={{ paddingHorizontal: 10 }}>
            <Text
              style={{
                color: "black",
                fontFamily: "OpenSans-Bold",
                fontSize: wp("3.7"),
                marginBottom: 10,
              }}
            >
              Order List
            </Text>
            {orderList.map((item, i) => {
              let detail = item.food_price.filter((e) => {
                return e.service_type_id === attribute.service_type;
              })[0];
              return (
                <View style={{ marginVertical: 3 }}>
                  <Text style={{ flex: 0.5, fontFamily: "OpenSans-SemiBold" }}>
                    {item.name}
                  </Text>
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ flex: 0.4 }}>
                      {RupiahFormat(detail.price)}
                    </Text>
                    <Text style={{ flex: 0.25 }}>x {item.qty}</Text>
                    <Text style={{ flex: 0.35 }}>
                      {RupiahFormat(item.qty * detail.price)}
                    </Text>
                  </View>
                </View>
              );
            })}
            <View
              style={{
                height: 2,
                backgroundColor: color.ligthGrey,
                width: "100%",
                marginVertical: 3,
              }}
            />
            <PriceDetail
              title={""}
              value={RupiahFormat(
                orderList
                  .map((item, i) => {
                    let detail = item.food_price.filter((e) => {
                      return e.service_type_id === attribute.service_type;
                    })[0];
                    return item.qty * detail.price;
                  })
                  .reduce(function (accumVariable, curValue) {
                    return accumVariable + curValue;
                  }, 0)
              )}
            />
            <PriceDetail
              title={"PPN"}
              value={RupiahFormat(
                (orderList
                  .map((item, i) => {
                    let detail = item.food_price.filter((e) => {
                      return e.service_type_id === attribute.service_type;
                    })[0];
                    return item.qty * detail.price;
                  })
                  .reduce(function (accumVariable, curValue) {
                    return accumVariable + curValue;
                  }, 0) *
                  10) /
                  100
              )}
            />
            <View
              style={{
                height: 2,
                backgroundColor: color.ligthGrey,
                width: "100%",
                marginVertical: 3,
              }}
            />
            <PriceDetail
              title={"Total"}
              value={RupiahFormat(
                (orderList
                  .map((item, i) => {
                    let detail = item.food_price.filter((e) => {
                      return e.service_type_id === attribute.service_type;
                    })[0];
                    return item.qty * detail.price;
                  })
                  .reduce(function (accumVariable, curValue) {
                    return accumVariable + curValue;
                  }, 0) *
                  10) /
                  100 +
                  orderList
                    .map((item, i) => {
                      let detail = item.food_price.filter((e) => {
                        return e.service_type_id === attribute.service_type;
                      })[0];
                      return item.qty * detail.price;
                    })
                    .reduce(function (accumVariable, curValue) {
                      return accumVariable + curValue;
                    }, 0)
              )}
            />
          </View>
          <View
            style={{
              height: 5,
              backgroundColor: color.ligthGrey,
              width: "100%",
              marginVertical: 20,
            }}
          />
          <View style={{ paddingHorizontal: 10 }}>
            <Picker
              title={"Payment Method"}
              onPress={() => {
                setActiveModal(true);
                setAttribute({ ...attribute, type_modal: "payment_method" });
              }}
              value={
                payment_type.filter((e) => {
                  return e.id === attribute.payment_type;
                })[0].name
              }
            />
            <View
              style={{
                marginVertical: 40,
                width: "100%",
                alignSelf: "center",
              }}
            >
              <Button
                title={"ORDER"}
                color={color.secondary}
                // small={true}
                borderRadius={100}
                fontSize={17}
                onPress={() => order()}
                loading={loading}
              />
            </View>
          </View>
          <View style={{ height: 50 }} />
        </ScrollView>
      </View>
      <Modal
        onClosed={[activeModal, setActiveModal]}
        title={
          attribute.type_modal == "payment_method"
            ? "Payement Method"
            : "Service Type"
        }
      >
        {attribute.type_modal == "service_type" &&
          service_type.map((item) => {
            return (
              <View>
                <RadioButton
                  title={item.name}
                  value={item.id}
                  data={attribute.service_type}
                  onPress={(e) => {
                    setAttribute({
                      ...attribute,
                      service_type: e,
                    });
                    setActiveModal(false);
                  }}
                />
              </View>
            );
          })}
        {attribute.type_modal == "payment_method" &&
          payment_type.map((item) => {
            return (
              <View>
                <RadioButton
                  title={item.name}
                  value={item.id}
                  data={attribute.payment_type}
                  onPress={(e) => {
                    setAttribute({
                      ...attribute,
                      payment_type: e,
                    });
                    setActiveModal(false);
                  }}
                />
              </View>
            );
          })}
      </Modal>
    </SafeAreaView>
  );
};

export default OrderType;
