export const color = {
  primary: "#00919F",
  background: "#ffffff",
  buttonColor: "#d5e000",
  secondary: "#F99500",
  secondaryLight: "#f0ca31",
  ligthGrey: "#eeeeee",
  Grey: "#999999",
  dragGrey: "#777777",
  white: "white",
  error: "red",
};

export const service_type = [
  { id: 1, name: "Dine in" },
  { id: 2, name: "Take away" },
];

export const payment_type = [
  { id: 1, name: "Tunai" },
  { id: 2, name: "Flazz BCA" },
  { id: 3, name: "EDC Mandiri" },
];
