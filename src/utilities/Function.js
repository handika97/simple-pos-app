export const sayTime = () => {
  let date = new Date();
  if (date.getHours() >= 5 && date.getHours() < 12) {
    return "Selamat Pagi";
  } else if (date.getHours() >= 12 && date.getHours() < 15) {
    return "Selamat Siang";
  } else if (date.getHours() >= 15 && date.getHours() < 18) {
    return "Selamat Sore";
  } else {
    return "Selamat Malam";
  }
};

export const PoliceNumberFormat = (value) => {
  return value ? value.replace(/([A-Za-z]{1,3})([0-9]{1,5})/g, "$1 $2 ") : "";
};

export const RupiahFormat = (value) => {
  if (!value) return "";
  let number_string = value.toString();
  let sisa = number_string.length % 3;
  let rupiah = number_string.substr(0, sisa);
  let ribuan = number_string.substr(sisa).match(/\d{3}/g);

  if (ribuan) {
    let separator = sisa ? "." : "";
    rupiah += separator + ribuan.join(".");
  }
  return "Rp, " + rupiah;
};
