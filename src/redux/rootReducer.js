import authReducer from "./features/authSlice";
import orderReducer from "./features/orderSlice";
import { combineReducers } from "@reduxjs/toolkit";

const rootReducer = combineReducers({
  auth: authReducer,
  orders: orderReducer,
});

export default rootReducer;
