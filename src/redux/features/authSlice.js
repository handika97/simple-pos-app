import { createSlice } from "@reduxjs/toolkit";
// import Geolocation from 'react-native-geolocation-service';
import { BaseUrl } from "../../utilities/BaseUrl";
const authSlice = createSlice({
  name: "auth",
  initialState: {
    Loading: false,
    data: {
      UserID: "",
      Role: "",
      UserName: "",
      latitude: -6.1975667,
      longitude: 106.8563727,
      updateApp: false,
      login_at: new Date(),
    },
    error: null,
    isLogin: false,
    headers: {
      token: "",
      "Content-Type": "application/json;charset=UTF-8",
      Accept: "application/json",
    },
  },
  reducers: {
    authLoginPending: (state) => {
      state.Loading = true;
    },
    authLoginRejected: (state, action) => {
      state.Loading = false;
      state.error = action.payload;
    },
    authLoginReceive: (state, action) => {
      state.isLogin = true;
      // state.Loading = false;
      state.data = action.payload;
      state.headers.token = action.payload.token;
    },
    authToken: (state, action) => {
      state.headers.token = action.payload.token;
    },
    onErrorRead: (state) => {
      state.error = null;
    },
    updateVersion: (state, action) => {
      state.updateApp = action.payload;
    },
    setPosition: (state, action) => {
      state.data.latitude = action.payload.latitude;
      state.data.longitude = action.payload.longitude;
    },
    authLogOut: (state) => {
      state.isLogin = false;
      state.data = {
        id: "",
        name: "",
        branch_id: "",
        role: "",
        login_at: "",
        token: "",
      };
      state.headers.token = "";
    },
  },
});

export const {
  authLoginPending,
  authLoginRejected,
  authLoginReceive,
  authLogOut,
  authToken,
  updateVersion,
  setPosition,
} = authSlice.actions;

export default authSlice.reducer;
import { post } from "./main";
const defaultBody = null;

export const authLogin = (email, password, error = () => {}) => {
  let bodyData = {
    email: email,
    password: password,
  };
  return (dispatch) => {
    dispatch(authLoginPending());
    dispatch(
      post(
        BaseUrl + "/auth/login",
        bodyData,
        (res) => {
          dispatch(authLoginReceive(res.data));
        },
        (err) => {
          dispatch(authLoginRejected(err.response.data.message));
          error();
        }
      )
    );
  };
};

export const authRegister = (name, email, password) => {
  let bodyData = {
    name: name,
    email: email,
    password: password,
  };
  return (dispatch) => {
    dispatch(
      post(
        "/admin/register",
        bodyData,
        (res) => {
          alert("success");
        },
        (err) => {
          alert("err");
        }
      )
    );
  };
};

export const onErrorReset = () => {
  return (dispatch) => {
    dispatch(onErrorRead());
  };
};
