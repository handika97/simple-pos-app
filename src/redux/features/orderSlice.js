import { createSlice } from "@reduxjs/toolkit";
// import Geolocation from 'react-native-geolocation-service';
import { BaseUrl } from "../../utilities/BaseUrl";
const orderSlice = createSlice({
  name: "orders",
  initialState: {
    orderList: [],
    totalOrder: 0,
  },
  reducers: {
    setPlus: (state, action) => {
      let index = state.orderList
        .map(function (e) {
          return e.menu_food_id;
        })
        .indexOf(action.payload.id);
      if (index >= 0) {
        state.orderList[index].qty++;
      } else {
        state.orderList = state.orderList.concat({
          menu_food_id: action.payload.id,
          qty: 1,
          code: action.payload.code,
          name: action.payload.name,
          food_price: action.payload.food_price,
        });
      }
      state.totalOrder++;
    },
    setMinus: (state, action) => {
      let index = state.orderList
        .map(function (e) {
          return e.menu_food_id;
        })
        .indexOf(action.payload.id);
      if (index >= 0) {
        if (state.orderList[index].qty > 1) {
          state.orderList[index].qty--;
        } else {
          state.orderList = state.orderList.filter((item, i) => {
            return item.menu_food_id != action.payload.id;
          });
        }
        if (state.totalOrder > 0) {
          state.totalOrder--;
        }
      }
    },
    setClean: (state, action) => {
      state.orderList = [];
      state.totalOrder = 0;
    },
  },
});

export const { setPlus, setMinus, setClean } = orderSlice.actions;

export default orderSlice.reducer;
import { post } from "./main";
const defaultBody = null;
