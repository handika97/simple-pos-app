import {configureStore} from '@reduxjs/toolkit';
import {persistStore, persistReducer} from 'redux-persist'; // defaults to localStorage for web
import {createLogger} from 'redux-logger';
import ReduxThunk from 'redux-thunk';
import AsyncStorage from '@react-native-community/async-storage';

import rootReducer from './rootReducer';

const persistConfig = {
  timeout: 10000,
  key: 'root',
  storage: AsyncStorage,
  // blacklist: ['gadai', 'nasabah']
};

const logger = createLogger({});
const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = configureStore({
  reducer: persistedReducer,
  middleware: [ReduxThunk],
  enhancers: (defaultEnhancers) => [...defaultEnhancers],
});

export const persistor = persistStore(store);

// persistor.purge()
