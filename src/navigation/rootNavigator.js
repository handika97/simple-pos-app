import React, { useState, useEffect } from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { useSelector } from "react-redux";
import { Login, Splash } from "../screen/auth";
import { MenuScreen, CartScreen } from "../screen/home/Task";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import { Text } from "react-native";
import { color } from "../utilities/json";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

const Stack = createStackNavigator();

function AppNavigator() {
  const { isLogin } = useSelector((state) => state.auth);
  const [performShow, setPerformShow] = useState(true);
  useEffect(() => {
    setTimeout(() => setPerformShow(false), 5500);
  }, []);

  return (
    <Stack.Navigator>
      {performShow && (
        <Stack.Screen
          name="Splash"
          component={Splash}
          options={{
            headerShown: false,
          }}
        />
      )}
      {!isLogin ? (
        <Stack.Screen
          name="Auth"
          component={Auth}
          options={{
            animationEnabled: false,
            headerShown: false,
          }}
        />
      ) : (
        <Stack.Screen
          name="HomePage"
          component={Home}
          options={{
            headerShown: false,
          }}
        />
      )}
    </Stack.Navigator>
  );
}

function Auth() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Login"
        component={Login}
        options={{
          animationEnabled: false,
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
}

function Home() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="MenuScreen"
        component={MenuScreen}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="CartScreen"
        component={CartScreen}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
}

export default AppNavigator;
