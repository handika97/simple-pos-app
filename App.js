import React from "react";
import { Provider } from "react-redux";
import { NavigationContainer } from "@react-navigation/native";
import { PersistGate } from "redux-persist/integration/react";

import { store, persistor } from "./src/redux/store";
import { navigationRef } from "./src/navigation/navigationService";
import AppNavigator from "./src/navigation/rootNavigator";
import { LogBox, Text } from "react-native";

LogBox.ignoreLogs(["Warning: ...", "Encountered", "Virtualized"]);

const App: () => React$Node = () => {
  return (
    <NavigationContainer ref={navigationRef}>
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <AppNavigator />
        </PersistGate>
      </Provider>
    </NavigationContainer>
  );
};

export default App;
